import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file=open(inputFilePath, 'r')
    lines=file.readlines()
    text=(lines[0])
    file.close()
    return True, text

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    n=len(text)
    t=len(password)
    i=0
    j=0
    encryptedText= ""
    for i in range (n):
        temp= ord(text[i]) + ord(password[j]) - 64
        encryptedText+= chr(temp)
        j+=1
        if j==(t-1):
            j=0


    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(encryptedText, password):
    n = len(encryptedText)
    t = len(password)
    i = 0
    j = 0
    decryptedText=""
    for i in range(n):
        temp= ord(encryptedText[i]) - ord(password[j]) + 64
        decryptedText+= chr(temp)
        j+=1
        if j == (t - 1):
            j = 0

    return True, decryptedText

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)


    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
